
const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

// USER REGISTRATION

router.post('/register', (req,res) => {
	userController.userSignup(req.body).then(result => res.send(result));
})


// USER AUTHENTICATION

router.post('/check', (req, res) => {
	userController.authenticate(req.body).then(result => res.send(result));
})


// SET USER AS ADMIN

router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		userController.setAdmin(req.params).then(result => res.send(result))
	}else{
		res.send(false)
	}
})


// MAKE AN ORDER

router.post('/placeOrder', (req,res) => {
	
	let data = {
		
		userId: auth.decode(req.headers.authorization).id,
		
		productId: req.body.productId,
		totalAmount: req.body.totalAmount

	}

	console.log(data)
	userController.checkOut(data).then(result => res.send(result))
})





// GET ORDERS

router.get('/:userId/orders' , (req,res) => {

let data = {
		
		userId: auth.decode(req.headers.authorization).id

		
	}

	userController.getOrder(req.body,req.params).then(result => res.send(result));

	

})






// GET ALL ORDERS

router.get('/allOrders' , auth.verify , (req,res) => {

const data = {
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {

	userController.getAllOrders().then(result => res.send(result));

	}

	else {

		res.send(false)
	}


})


// CHANGE PASSWORD

/*
router.put('/:userId/change' , (req,res) => {

let data = {
		
		userId: auth.decode(req.headers.authorization).id

		
	}

	userController.changePassword(req.body,req.params).then(result => res.send(result));

	

})
*/



module.exports = router;
