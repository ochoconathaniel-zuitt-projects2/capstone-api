

const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');

// CREATE A PRODUCT

router.post('/create', auth.verify, (req,res) => {

	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	
	}

	if(data.isAdmin){
		
		productController.createItem(req.body).then(result => res.send(result))
	
	}else{
		
		return ('you are not an admin !');
	}
})


// GET ALL ACTIVE PRODUCTS

router.get('/active', (req, res) =>{
	productController.getAllActiveProducts().then(result => res.send(result))
})


// GET SPECIFIC PRODUCT

router.get('/:productId', (req, res) =>{
	productController.getSpecific(req.params).then(result => res.send(result))
})


// UPDATE A PRODUCT

router.put('/:productId/updateItem', auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

		productController.updateItem(req.params,req.body).then(result => res.send(result))
	} 
	else {

		return ('you are not an admin !');
	}

})

// ARCHIVE A PRODUCT

router.put('/:productId/archive', auth.verify, (req,res) => {



	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

		productController.archiveProduct(req.params).then(result => res.send(result))
	} 
	else {

		return ('you are not an admin !');
	}


})


// DELETE A PRODUCT

router.delete('/:productId/removeItem', auth.verify , (req,res) => {

		const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

		productController.deleteItem(req.params).then(result => res.send(result))
	} 
	else {

		return ('you are not an admin !');
	}


})













module.exports = router;
