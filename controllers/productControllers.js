
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// CREATE A PRODUCT

module.exports.createItem = (reqBody) =>{


	let newProduct = new Product({
		
		productName: reqBody.productName,
		
		productPrice: reqBody.productPrice,
		
		productDescription: reqBody.productDescription
	
	})


	return newProduct.save().then((product, error) => {
		
		if(error){
			return false;
		}else{
	
			return ('Item successfully created');
		}
	})
}

// GET ALL ACTIVE PRODUCTS

module.exports.getAllActiveProducts= () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// GET A SPECIFIC PRODUCT

module.exports.getSpecific = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;

	})
}


// UPDATE A PRODUCT


module.exports.updateItem = (reqParams,reqBody) => {
	
	let updatedProduct = {
		productName: reqBody.productName,
		productPrice: reqBody.productPrice,
		productDescription: reqBody.productDescription
	};

return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) => {

	if(error){

		return ('Update Unsuccesful');
	}

	else {

		return ('Item successfully updated');
	}
})

}

// ARCHIVE A PRODUCT

module.exports.archiveProduct = (reqParams) => {

	let updatedProduct = {
		isActive: false
	}
	
	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return ('Item archiving unsuccesful');
		}else{
			return ('Item has been archived');
		}
	})
}


// DELETE A PRODUCT

module.exports.deleteItem = (reqParams) => {
return Product.findByIdAndRemove(reqParams.productId).then((deleted,error) => {
	if(error){
		return ('Failed to delete item!');
	} 

	else {
		return ('Item successfully deleted!');
	}
})

}

